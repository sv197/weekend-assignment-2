//
//  ViewController.swift
//  Weekend Assignment 2
//
//  Created by The App Experts on 06/03/2020.
//  Copyright © 2020 The App Experts. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet var nameTextField: UITextField!
    @IBOutlet var numberTextField: UITextField!
    @IBOutlet var tableView: UITableView!
    
    
    var model: ContactsModel!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        model = ContactsModel()        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.tableFooterView = UIView()
    }
    
    @IBAction func addContact(_ sender: Any) {
        guard let name = nameTextField.text else {
            return
        }
        guard let number = numberTextField.text else {
            return
        }
        nameTextField.text = ""
        numberTextField.text = ""
        var newContact: Contact
        if name == "" || name.starts(with: " ") {
            let alert = UIAlertController(title: "Invalid Name", message: "Please enter a valid name", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            //            alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
            
            self.present(alert, animated: true)
            
        } else {
            if number == "" || number.starts(with: " ") {
                newContact = Contact(name: name)
            } else {
                newContact = Contact(name: name, phoneNumber: number)
            }
            model.add(newContact)
            tableView.reloadData()
        }
        
    }
    
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return model.sections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.numberOfRowsInSection(section)
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        return String(model.sectionNames()[section])
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ContactsTableViewCell
        
        guard let item = model.item(at: indexPath) else {
            return cell
        }
        
        cell.nameLabel.text = item.name
        if let number = item.phoneNumber {
            cell.numberLabel.text = number
            cell.phoneImageView.isHidden = false
        } else {
            cell.numberLabel.text = nil
            cell.phoneImageView.isHidden = true
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        tableView.rowHeight = 75
        return tableView.rowHeight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selected = model.item(at: indexPath) ?? nil
        guard let name = selected?.name else {
            return
        }
        guard let number = selected?.phoneNumber else {
            let alert = UIAlertController(title: "No Phone Number", message: "The contact has no phone number", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true)
            return
        }
        let alertMessage: String = "Hey, \(name) just met you, and here's their number \(number) so call them maybe?"
        let myActionSheet =  UIAlertController(title: "Call Them Maybe?", message: alertMessage, preferredStyle: UIAlertController.Style.actionSheet)
        myActionSheet.addAction(UIAlertAction(title: "Via Phone", style: UIAlertAction.Style.default, handler: nil))
        myActionSheet.addAction(UIAlertAction(title: "Via Skype", style: UIAlertAction.Style.default, handler: nil))
        myActionSheet.addAction(UIAlertAction(title: "Via What'sApp", style: UIAlertAction.Style.default, handler: nil))
        myActionSheet.addAction(UIAlertAction(title: "How About No", style: UIAlertAction.Style.destructive, handler: nil))
        self.present(myActionSheet, animated: true, completion: nil)
    }
}
