//
//  ContactsTableViewCell.swift
//  Weekend Assignment 2
//
//  Created by The App Experts on 07/03/2020.
//  Copyright © 2020 The App Experts. All rights reserved.
//

import UIKit

class ContactsTableViewCell: UITableViewCell {
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var numberLabel: UILabel!
    @IBOutlet var phoneImageView: UIImageView!
    @IBOutlet var contactImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
