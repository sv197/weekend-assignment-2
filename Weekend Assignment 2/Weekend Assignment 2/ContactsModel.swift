//
//  ContactsModel.swift
//  Weekend Assignment 2
//
//  Created by The App Experts on 06/03/2020.
//  Copyright © 2020 The App Experts. All rights reserved.
//

import UIKit

struct Contact {
    var name: String
    var phoneNumber: String?
}

class ContactsModel {
    
    var contacts: [Contact] = []
    
    func add(_ contact: Contact) {
        contacts.append(contact)
        contacts = contacts.sorted(by: {$0.name < $1.name})
    }
}

extension ContactsModel {
    
    func sectionNames() -> [Character] {
        var filteredArray: [Character] = []
        
        for i in 0..<contacts.count {
            guard let x = contacts[i].name.first else {
                break
            }
            
            if !filteredArray.contains(Character(x.uppercased())) {
                filteredArray.append(Character(x.uppercased()))
            }
            
        }
        return filteredArray
    }
    
    var sections: Int {
        return sectionNames().count
    }
    
    func itemsInSection(_ section: Int) -> [Contact] {
        let s = sectionNames()[section]
        var filteredArray: [Contact] = []
        for i in 0..<contacts.count {
            contacts[i].name = contacts[i].name.capitalized
            let x = contacts[i]
            
            if x.name.first == s {
                filteredArray.append(contacts[i])
            }
        }
        return filteredArray
    }
    
    func numberOfRowsInSection(_ section: Int) -> Int {
        let filteredArray = itemsInSection(section)
        return filteredArray.count
    }
    
    func item(at indexPath: IndexPath) -> Contact? {
        if indexPath.section < 0 || indexPath.section > sections {
            return nil
        }
        if indexPath.row < 0 || indexPath.row > numberOfRowsInSection(indexPath.section) {
            return nil
        }
        let items = itemsInSection(indexPath.section)
        return items[indexPath.row]
    }
    
}
